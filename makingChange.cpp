/*************************************************************
* Summary: Use Dynamic Programming to solve the making change
*          problem in 3 ways, bottom-up with memoization,
*          recursive with memoization,
*          and recursive without memoization.
* 
* Author: James Osborne and Nate Stahlnecker
* Created: 10/10/2017
**************************************************************/

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <climits>
#include <ctime>
#include "makingChange.h"

using namespace std;

int main(int argc, char** argv) {
    ifstream input;
    ofstream output;

    if (argc < 4) {
        cout << "Usage: [executable name] [input file] [output file] [flags]\n"
             << "\nFlag options:\n"
             << "\t-bu  (bottom-up)\n"
             << "\t-rm  (recursive with memoization)\n"
             << "\t-rnm (recursive without memoization)\n";

        return 1;
    }

    //Open input file and check if open succeeded
    input.open(argv[1]);
    if (!input.is_open()) {
        cout << "Input file could not open\n";

        return 2;
    }

    //Open output file and check if open succeeded
    output.open(argv[2]);
    if (!output.is_open()) {
        cout << "Output file could not open\n";

        return 3;
    }

    vector<int> denominations;
    vector<int> problems;
    string line;

    //Read number of coin denominations
    getline(input, line);
    int numberOfDenominations = atoi(line.c_str());
    
    //Grab all coin denominations
    for (int i = 0; i < numberOfDenominations; ++i) {
        getline(input, line);

        denominations.push_back(atoi(line.c_str()));
    }

    //Read number of problems
    getline(input, line);
    int numberOfProblems = atoi(line.c_str());

    //Grab all problems
    for (int i = 0; i < numberOfProblems; ++i) {
        getline(input, line);
        problems.push_back(atoi(line.c_str()));
    }

    //Loop through remaining flags and perform action specified
    for (int i = 3; i < argc; ++i) {
        performFlagAction(argv[i], &denominations, &problems, &output);
    }

    //Close input and output files
    input.close();
    output.close();

    return 0;
}

void bottomUp(vector<int>* denominations, int value, solutionData* memo) {
    //Initial base case of value u will have zero coins used
    memo[0].coinsUsed = 0;
    memo[0].coins = new int[denominations->size()];

    //Initialize coin counts for first spot
    for (int i = 0; i < denominations->size(); ++i) {
        memo[0].coins[i] = 0;
    }

    //Loop from the bottom up through memoization array
    for (int i = 1; i <= value; ++i) {
        int coinUsed = 0;
        //Current coin not known, so INT_MAX is used as default
        memo[i].coinsUsed = INT_MAX;

        for (int j = 0; j < denominations->size(); ++j) {
            //Denomination will only matter if it is >= current value
            if (i >= denominations->at(j)) {
                /*If a previous found solution is less than current solution,
                  grab the solution. This will always happen on first found
                  denomination due to memo[i].coinsUsed = INT_MAX*/
                if (memo[i - denominations->at(j)].coinsUsed
                        < memo[i].coinsUsed) {
                    //Track what coin has been chosen
                    coinUsed = j;

                    //Grab better solution from before and add a coin
                    memo[i].coinsUsed =
                        memo[i - denominations->at(j)].coinsUsed + 1;
                    memo[i].coins =
                        copyArray(memo[i - denominations->at(j)].coins,
                                  denominations->size());
                }
            } 
        }

        //Add chosen coin to its corresponding spot
        memo[i].coins[coinUsed]++;
    }
}

void cleanUpMemo(solutionData* memo, int size) {
    for (int i = 0; i < size; ++i) {
        if (memo[i].coins != NULL) {
            delete memo[i].coins;
            memo[i].coins = NULL;
        }
    }
}

int* copyArray(int* array, int size) {
    int* newArray = new int[size];

    //Loop through and copy all elements over
    for (int i = 0; i < size; ++i) {
        newArray[i] = array[i];
    }

    //Return deep copied array
    return newArray;
}

void makeChangeBottomUp(vector<int>* denominations,
                        int value,
                        ofstream* output) {
    //Create memo array
    solutionData memo[value + 1];

    double start = clock();
    bottomUp(denominations, value, memo);
    double finish = clock();
    
    double duration = (finish - start) / (double) CLOCKS_PER_SEC;
    
    printSolution(&(memo[value]), denominations, value, duration, output);

    cleanUpMemo(memo, value);
}

void makeChangeRecursiveMemo(vector<int>* denominations,
                             int value,
                             ofstream* output) {
    //Create memo array
    solutionData memo[value + 1];
    
    //Initialize all memo locations with coins array and a max coin count
    for (int i = 0; i <= value; ++i) {
        memo[i].coins = new int[denominations->size()];
        memo[i].coinsUsed = INT_MAX;
    }

    //Initialize base of memo array with the base required coins
    memo[0].coinsUsed = 0;
    for (int i = 0; i < denominations->size(); ++i) {
        memo[0].coins[i] = 0;
    }

    double start = clock();
    recursiveMemo(denominations, value, memo);
    double finish = clock();

    double duration = (finish - start) / (double) CLOCKS_PER_SEC;

    printSolution(&(memo[value]), denominations, value, duration, output);

    cleanUpMemo(memo, value);
}

void makeChangeRecursiveNoMemo(vector<int>* denominations,
                               int value,
                               ofstream* output) {
    solutionData solution;
    
    //Initialize the solution to be passed through recursive calls
    solution.coins = new int[denominations->size()];
    solution.coinsUsed = INT_MAX;

    for (int i = 0; i < denominations->size(); ++i) {
        solution.coins[i] = 0;
    }

    double start = clock();
    solution = recursiveNoMemo(denominations, value, solution);
    double finish = clock();

    double duration = (finish - start) / (double) CLOCKS_PER_SEC;

    printSolution(&solution, denominations, value, duration, output);

    delete solution.coins;
    solution.coins = NULL;
}

void performFlagAction(string flag,
                       vector<int>* denominations,
                       vector<int>* problems,
                       ofstream* output) {
    //Perform bottom-up
    if (flag == "-bu") {
        performMakeChange(denominations,
                          problems,
                          output,
                          makeChangeBottomUp);
    //Perform recursive with memo
    } else if (flag == "-rm") {
        performMakeChange(denominations,
                          problems,
                          output,
                          makeChangeRecursiveMemo);
    //Perform recursive without memo
    } else if (flag == "-rnm") {
        performMakeChange(denominations,
                          problems,
                          output,
                          makeChangeRecursiveNoMemo);
    //Invalid flag
    } else {
        cout << flag << " is an invalid flag\n";
    }
}

void performMakeChange(vector<int>* denominations,
                       vector<int>* problems,
                       ofstream* output,
                       void (*makeChange) (vector<int>* denominations,
                                           int value,
                                           ofstream* output)) {
    //Loop through problems and call the makeChange function passed in
    for (int i = 0; i < problems->size(); ++i) {
        makeChange(denominations, problems->at(i), output);
    }
}

void printSolution(solutionData* solution,
                   vector<int>* denominations,
                   int value,
                   double time,
                   ofstream* output) {
    *output << value << " cents = ";

    for (int i = denominations->size() - 1; i >= 0; --i) {
        if (solution->coins[i] > 0) {
            *output << denominations->at(i) << ":" << solution->coins[i] << " ";
        }
    }

    *output << "\nTime Taken: " << time << "\n\n";
}

void recursiveMemo(vector<int>* denominations, int value, solutionData* memo) {
    int coinUsed = 0;

    //If not the base case, recurse through and find solution
    if (value > 0) {
        for (int j = 0; j < denominations->size(); ++j) {
            //Denomination will only matter if it is >= current value
            if (value >= denominations->at(j)) {
                //If previous location not calculated, recurse
                if (memo[value - denominations->at(j)].coinsUsed == INT_MAX) {
                    recursiveMemo(denominations,
                                  value - denominations->at(j),
                                  memo);
                }
                /*If a previous found solution is less than current solution,
                  grab the solution. This will always happen on first found
                  denomination*/
                if (memo[value - denominations->at(j)].coinsUsed
                        < memo[value].coinsUsed) {
                    //Track what coin has been chosen
                    coinUsed = j;

                    //Grab better solution from before and add a coin
                    memo[value].coinsUsed =
                        memo[value - denominations->at(j)].coinsUsed + 1;
                    memo[value].coins =
                        copyArray(memo[value - denominations->at(j)].coins,
                                  denominations->size());
                }
            } 
        }

        //Add chosen coin to its corresponding spot
        memo[value].coins[coinUsed]++;
    }
}

solutionData recursiveNoMemo(vector<int>* denominations,
                             int value,
                             solutionData currentBestSolution) {
    solutionData solution;

    solution.coins = new int[denominations->size()];
    solution.coinsUsed = 0;

    for (int i = 0; i < denominations->size(); ++i) {
        solution.coins[i] = 0;
    }

    bool swapped = false;
    int coinUsed = -1;

    //If not the base case, recurse through and find the solution
    if (value > 0) {
        for (int j = 0; j < denominations->size(); ++j) {
            //Denomination will only matter if it is >= current value
            if (value >= denominations->at(j)) {
                //Recurse through current case minus the current denomination
                solution = recursiveNoMemo(denominations,
                                           value - denominations->at(j),
                                           currentBestSolution);
                //Track what coin has been chosen
                coinUsed = j;

                /*Check if current solution, with the extra coin, is greated
                  than the currentBestSolution*/
                if (solution.coinsUsed + 1 < currentBestSolution.coinsUsed) {
                    //Reset currentBestSolution to current solution
                    currentBestSolution = solution;
                    //Add chosen coin to its corresponding spot
                    solution.coins[coinUsed]++;
                } else {
                    solution = currentBestSolution;
                }
            } 
        }

        solution.coinsUsed++;
    }

    return solution;
}
